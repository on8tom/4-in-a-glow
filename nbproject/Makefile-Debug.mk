#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=avr-gcc
CCC=avr-g++
CXX=avr-g++
FC=gfortran
AS=avr-as

# Macros
CND_PLATFORM=arduino-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/project.o \
	${OBJECTDIR}/score.o \
	${OBJECTDIR}/matrixDriver.o \
	${OBJECTDIR}/game.o


# C Compiler Flags
CFLAGS=-Os -Wall -ffunction-sections -fdata-sections -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=100 -I/usr/share/arduino/hardware/arduino/variants/mega

# CC Compiler Flags
CCFLAGS=-c -g -Os -Wall -fno-exceptions -ffunction-sections -fdata-sections -mmcu=atmega2560 -DF_CPU=16000000L -MMD -DUSB_VID=null -DUSB_PID=null -DARDUINO=101 -I/usr/share/arduino/hardware/arduino/cores/arduino -I/usr/share/arduino/hardware/arduino/variants/mega
CXXFLAGS=-c -g -Os -Wall -fno-exceptions -ffunction-sections -fdata-sections -mmcu=atmega2560 -DF_CPU=16000000L -MMD -DUSB_VID=null -DUSB_PID=null -DARDUINO=101 -I/usr/share/arduino/hardware/arduino/cores/arduino -I/usr/share/arduino/hardware/arduino/variants/mega

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-larduino_corelib -lm

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk /tmp/${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/arduino-on8tom

/tmp/${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/arduino-on8tom: ${OBJECTFILES}
	${MKDIR} -p /tmp/${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o /tmp/${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/arduino-on8tom ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/project.o: project.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -I/usr/share/arduino/hardware/arduino/cores/arduino -I/usr/share/arduino/libraries -MMD -MP -MF $@.d -o ${OBJECTDIR}/project.o project.cpp

${OBJECTDIR}/score.o: score.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -I/usr/share/arduino/hardware/arduino/cores/arduino -I/usr/share/arduino/libraries -MMD -MP -MF $@.d -o ${OBJECTDIR}/score.o score.cpp

${OBJECTDIR}/matrixDriver.o: matrixDriver.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -I/usr/share/arduino/hardware/arduino/cores/arduino -I/usr/share/arduino/libraries -MMD -MP -MF $@.d -o ${OBJECTDIR}/matrixDriver.o matrixDriver.cpp

${OBJECTDIR}/game.o: game.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -I/usr/share/arduino/hardware/arduino/cores/arduino -I/usr/share/arduino/libraries -MMD -MP -MF $@.d -o ${OBJECTDIR}/game.o game.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} /tmp/${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/arduino-on8tom

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
