#include "project.h"
#include "game.h"


//Timer2 Overflow Interrupt Vector, called every 1ms
ISR(TIMER2_OVF_vect) {
  matrixDriver.drive();
  scoreMatrix.drive();

  TCNT2 = 100;           //Reset Timer to 130 out of 255
  TIFR2 = 0x00;          //Timer2 INT Flag Reg: Clear Timer Overflow Flag
};


void setup()
{

  TCCR2B = 0x00;        //Disbale Timer2 while we set it up
  TCNT2  = 100;         //Reset Timer Count to 130 out of 255
  TIFR2  = 0x00;        //Timer2 INT Flag Reg: Clear Timer Overflow Flag
  TIMSK2 = 0x01;        //Timer2 INT Reg: Timer2 Overflow Interrupt Enable
  TCCR2A = 0x00;        //Timer2 Control Reg A: Normal port operation, Wave Gen Mode normal
  TCCR2B = 0x05;        //Timer2 Control Reg B: Timer Prescaler set to 128
  Serial.begin(9600);

  bootSplash();

  game.start();
}

void loop()
{
  //bootSplash();
  game.buttonRead();

}

void bootSplash(){
  for(int i = 0; i < 3; i++){
    for(uint8_t i = 0; i < 8; i++){
      for(uint8_t j = 0; j < 8; j++){
        redMatrix.playerMatrix()->writeXY(j,i,1);
        redMatrix.draw();
        greenMatrix.playerMatrix()->writeXY(7-j,7-i,1);
        greenMatrix.draw();
        delay(5);
      }
      scoreMatrix.greenScore = i;
      scoreMatrix.redScore = 7-i;
    }

    for(uint8_t i = 0; i < 8; i++){
      for(uint8_t j = 0; j < 8; j++){
        redMatrix.playerMatrix()->writeXY(j,i,0);
        redMatrix.draw();
        greenMatrix.playerMatrix()->writeXY(7-j,7-i,0);
        greenMatrix.draw();
        delay(5);
      }
      scoreMatrix.greenScore  = 7-i;
      scoreMatrix.redScore = i;
    }
  }
  scoreMatrix.redScore = 0;
}