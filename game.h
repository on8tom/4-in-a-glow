/*
 * File:   game.h
 * Author: on8tom
 *
 * Created on February 17, 2013, 2:06 PM
 *
 *
 */

#ifndef GAME_H
#define	GAME_H

#include <Arduino.h>

#include "matrixDriver.h"

class Game {
public:
  Game(
          uint8_t b1,
          uint8_t b2,
          uint8_t b3,
          uint8_t b4,
          doubleFrameBuffer* greenMatrix,
          doubleFrameBuffer* redmatrix);
  void start();
  void buttonRead();
private:
  enum player{
    red=0,
    green=1,
  };
  uint8_t buttons[4];
  uint32_t nextTime[4];

  uint8_t _playDirection;
  player CurrentPlayer : 1;
  player beginPlayer : 1;

  doubleFrameBuffer* currentMatrix;
  doubleFrameBuffer* _greenMatrix;
  doubleFrameBuffer* _redMatrix;

  void buttonEvent(uint8_t button);

  uint8_t rowPlace;

  void drawRow(bool on);
  void drop();
  void checkWon(uint8_t x, uint8_t y);

  struct{
    uint8_t x0 : 3;
    uint8_t y0 : 3;
    uint8_t x1 : 3;
    uint8_t y1 : 3;
  } coords[4];

  void won(uint8_t nWon);

  void currpl();
};

#endif	/* GAME_H */

