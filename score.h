/*
 * File:   score.h
 * Author: on8tom
 *
 * Created on February 16, 2013, 7:16 PM
 *
 *
 */

#ifndef SCORE_H
#define	SCORE_H

#include <Arduino.h>

class score {
public:
  score(uint8_t, uint8_t, uint8_t);
  void drive();
  void test();
  void setPlayers(uint8_t* greenScore, uint8_t * redScore );
  uint8_t greenScore : 3;
  uint8_t redScore : 3;
private:
  uint8_t _side : 1;
  volatile uint8_t* _barGraphPort;
  uint8_t _redPin;
  uint8_t _greenPin;
};

#endif	/* SCORE_H */

