/*
 * File:   game.cpp
 * Author: on8tom
 *
 * Created on February 17, 2013, 2:06 PM
 */

#include "game.h"
#include "score.h"

Game::Game(uint8_t b1,
        uint8_t b2,
        uint8_t b3,
        uint8_t b4,
        doubleFrameBuffer* greenMatrix,
        doubleFrameBuffer* redMAtrix){
  buttons[0] = b1;
  buttons[1] = b2;
  buttons[2] = b3;
  buttons[3] = b4;

  pinMode(buttons[0], INPUT);
  pinMode(buttons[1], INPUT);
  pinMode(buttons[2], INPUT);
  pinMode(buttons[3], INPUT);

  digitalWrite(buttons[0], HIGH);
  digitalWrite(buttons[1], HIGH);
  digitalWrite(buttons[2], HIGH);
  digitalWrite(buttons[3], HIGH);

  _greenMatrix = greenMatrix;
  _redMatrix = redMAtrix;

  _playDirection = 0;

}

void Game::start(){
  //wipe screen
  _greenMatrix->playerMatrix()->wipe();
  _greenMatrix->draw();
  _redMatrix->playerMatrix()->wipe();
  _redMatrix->draw();

  rowPlace = 3;

  currentMatrix = _redMatrix;
  drawRow(1);
}

void Game::buttonRead(){
  for(uint8_t i = 0; i< sizeof(buttons); i++){
    if(digitalRead(buttons[i]) == 0){
      if(millis() < nextTime[i])
        continue;
      buttonEvent(i);
      nextTime[i] = millis() + 500;
    };

  }
}

void Game::buttonEvent(uint8_t button){
  drawRow(0);
  Serial.println(button,DEC);
  switch(button){
    case 1:
      rowPlace -= (rowPlace == 0) ? 0 : 1;
      drawRow(1);
      break;
    case 2:
    case 0:
      drop();
      break;
    case 3:
      rowPlace += (rowPlace == 6) ? 0 : 1;
      drawRow(1);
      break;
  }
}

void Game::drop(){
  if(_greenMatrix->displayMatrix()->readXY(rowPlace,0) ||
     _redMatrix->displayMatrix()->readXY(rowPlace,0))
    return;

  uint8_t i;

  for(i=0; i<7 ; i++){

    if(_greenMatrix->displayMatrix()->readXY(rowPlace,i+1) ||
     _redMatrix->displayMatrix()->readXY(rowPlace,i+1))
      break;

    currentMatrix->playerMatrix()->writeXY(rowPlace,i,0);
    currentMatrix->playerMatrix()->writeXY(rowPlace,i+1,1);
    //delay(10);
    currentMatrix->draw();
  }

  checkWon(rowPlace,i);

  CurrentPlayer ++;
  currpl();
  drawRow(1);
}
void Game::drawRow(bool on){
  uint8_t x,y;
  switch(_playDirection){
    case 0:
      x = rowPlace;
      y = 0;
      break;
    case 1:
      x = 7;
      y = rowPlace;
      break;
    case 2:
      x = 7- rowPlace;
      y = 7;
      break;
    case 3:
      x = 0;
      y = 7- rowPlace +1;
  }
  switch(CurrentPlayer){
    case green:
      _greenMatrix->playerMatrix()->writeXY(x,y,on);
      _greenMatrix->draw();
      break;
    case red:
      _redMatrix->playerMatrix()->writeXY(x,y,on);
      _redMatrix->draw();
      break;
  }
}

void Game::currpl(){
  switch(CurrentPlayer){
    case green:
      currentMatrix = _greenMatrix;
      break;
    case red:
      currentMatrix = _redMatrix;
      break;
  }
}

void Game::checkWon(uint8_t x, uint8_t y){
  uint8_t n = 0;
  uint8_t nWon = 0;
  //check |
  for(int8_t i = y; i <= 7 ; i++){
    if(currentMatrix->displayMatrix()->readXY(x,i))
      n++;
    else
      break;
  }
  if(n >= 4){
    coords[nWon].x0 = x;
    coords[nWon].y0 = y;
    coords[nWon].x1 = x;
    coords[nWon].y1 = y + n -1;
    nWon++ ;
  }

  n = 0;
  //check -
  for(int8_t i = x; i >= 0 ; i--){
    if(currentMatrix->displayMatrix()->readXY(i,y))
      n++;
    else{
      coords[nWon].x0 = i + 1;
      break;
    }
  }
  for(int8_t i = x; i <= 6 ; i++){
    if(currentMatrix->displayMatrix()->readXY(i,y))
      n++;
    else{
      coords[nWon].x1 = i - 1;
      break;
    }
  }
  if(n > 4){
    coords[nWon].y0 = y;
    coords[nWon].y1 = y;
    nWon++ ;
  }

  //check /
  for(int8_t i = x, j = y; i >= 0 && j >0 ; i--, j--){
    if(currentMatrix->displayMatrix()->readXY(i,y))
      n++;
    else{
      coords[nWon].x0 = i + 1;
      coords[nWon].y0 = j + 1;
      break;
    }
  }
  for(int8_t i = x, j = y; i <= 6 && j <= 7 ; i++, j++){
    if(currentMatrix->displayMatrix()->readXY(i,y))
      n++;
    else{
      coords[nWon].x1 = i - 1;
      coords[nWon].y1 = j - 1;
      break;
    }
  }
  if(n > 4){
    nWon++ ;
  }

  //check \ .
  for(int8_t i = x, j = y; i >= 0 && j <=7 ; i--, j++){
    if(currentMatrix->displayMatrix()->readXY(i,y))
      n++;
    else{
      coords[nWon].x0 = i + 1;
      coords[nWon].y0 = j - 1;
      break;
    }
  }
  for(int8_t i = x, j = y; i <= 6 && j > 0 ; i++, j--){
    if(currentMatrix->displayMatrix()->readXY(i,y))
      n++;
    else{
      coords[nWon].x1 = i - 1;
      coords[nWon].y1 = j + 1;
      break;
    }
  }
  if(n > 4){
    nWon++ ;
  }

  if(nWon)
    won(nWon);
}

void Game::won(uint8_t nWon){
  for(uint8_t i = 0; i < nWon; i++){
    for(uint8_t j = coords[nWon].x0; j <= coords[nWon].x1; j++ ){
      for(uint8_t k = coords[nWon].y0; k <= coords[nWon].y1; k++)
        currentMatrix->playerMatrix()->writeXY(j, k, 0);
    }
  }

  //blink routine
  for(;;){
    bool Break = false;

    //off
    currentMatrix->swap();

    uint32_t next = millis() + 250;
    while(millis() < next){
      for(uint8_t i = 0; sizeof(buttons); i++){
        if(!digitalRead(buttons[i])){
          Break = true;
          break;
        }
      }
    }

    //on
    currentMatrix->swap();

    next = millis() + 250;
    while(Break == true && millis() < next){
      for(uint8_t i = 0; sizeof(buttons); i++){
        if(!digitalRead(buttons[i])){
          break;
        }
      }
    }

  }



  switch(CurrentPlayer){
    case red:
      score.redScore ++;
      break;
    case green:
      score.greenScore ++;
  }
}