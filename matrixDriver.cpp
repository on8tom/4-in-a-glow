/*
 * File:   matrixDriver.cpp
 * Author: on8tom
 *
 * Created on February 15, 2013, 11:50 PM
 */

#include "matrixDriver.h"

matrix::matrix(){
  wipe();
}

void matrix::writeRow(uint8_t index, uint8_t data){
  if(index >=8)
    return;

  m[index] = data;
}

void matrix::writeColumn(uint8_t index, uint8_t data){
  if(index >=8)
    return;

  // 0 right lsb
  // 7 left msb
  uint8_t bitmask = 0x01 << index;

  for(uint8_t i = 0; i < 8; i++){
    //0 top lsb
    //7 bottom msb
    if(data & (0x01 << i))
      m[i] |= bitmask;
    else
      m[i] &= ~bitmask;
  }
}

void matrix::writeXY(uint8_t x, uint8_t y, uint8_t data){
  /*
   * x: column
   *    0 left msb
   *    7 right lsb
   *
   * y: row
   *    0 top lsb
   *    7 bottom msb
   */

  if(x >= 8)
    return;
  if(y >= 8)
    return;

  uint8_t bitmask = 0x80 >> x;

  if(data)
    m[y] |= bitmask;
  else
    m[y] &= ~bitmask;
}

uint8_t matrix::readRow(uint8_t index){
  if(index >=8)
    return 0;

  return m[index];
}

uint8_t matrix::readColumn(uint8_t index){
  if(index >=8)
    return 0;

  uint8_t bitmask = 0x80 >> index;
  uint8_t returnValue = 0x00;

  for(uint8_t  i = 0; i < 8; i++){
    if(m[i] & bitmask)
      returnValue |= (0x01 << i);
  }
  return returnValue;
}

uint8_t matrix::readXY(uint8_t x, uint8_t y){
  /*
   * x: column
   *    0 left msb
   *    7 right lsb
          *
   * y: row
   *    0 top lsb
   *    7 bottom msb
   */
  if(x >= 8)
    return 0;
  if(y >=8)
    return 0;

  if(m[y] & (0x80 >> x))
    return 1;
  else
    return 0;
}

void matrix::wipe(){
  memset(m,0,8);
}

matrixDriver::matrixDriver(uint8_t commonPin, uint8_t greenPin, uint8_t redPin,
        doubleFrameBuffer* greenMAtrix, doubleFrameBuffer* redMatrix){
  //find port registers
  uint8_t arduinoPort = digitalPinToPort(commonPin);
  PORTcommon = portOutputRegister(arduinoPort);

  //make all output (ddr high)
  * portModeRegister(arduinoPort) = 0xff;

  arduinoPort = digitalPinToPort(greenPin);
  PORTgreen = portOutputRegister(arduinoPort);
  * portModeRegister(arduinoPort) = 0xff;

  arduinoPort = digitalPinToPort(redPin);
  PORTred = portOutputRegister(arduinoPort);
  * portModeRegister(arduinoPort) = 0xff;

  _greenMatrix = greenMAtrix;
  _redMatrix = redMatrix;
}

void matrixDriver::drive(void){
  static uint8_t i = 0;

  //bitmask row
  uint8_t rowBitmask  = 0x01 << i;
  //write green and red column
  *PORTgreen = ~ (*_greenMatrix).displayMatrix()->m[i];
  *PORTred = ~ (*_redMatrix).displayMatrix()->m[i];
  //activate row
  *PORTcommon = rowBitmask;
  // delay for persistence of vision
  delayMicroseconds(500);
  //deactivate row;
  *PORTcommon = 0x00;

  i++;
  if(i >=8)
    i=0;
}

void doubleFrameBuffer::swap(){
  if(mplayerMatrix == & m0){
    mplayerMatrix = &m1;
    mdisplayMatrix = &m0;
  }
  else{
    mplayerMatrix = &m0;
    mdisplayMatrix = &m1;
  }
}

doubleFrameBuffer::doubleFrameBuffer(){
  mplayerMatrix = & m0;
  mdisplayMatrix = & m1;
}

void doubleFrameBuffer::draw(){
  swap();
  memcpy(mplayerMatrix->m, mdisplayMatrix->m, sizeof(mplayerMatrix->m));
}