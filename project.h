/*
 * File:   project.h
 * Author: on8tom
 *
 * Created on February 16, 2013, 1:51 AM
 */

#ifndef PROJECT_H
#define	PROJECT_H
#include <Arduino.h>
#include "matrixDriver.h"
#include "project.h"
#include "score.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include "game.h"

doubleFrameBuffer greenMatrix, redMatrix;

matrixDriver matrixDriver(25,35,45,
        & greenMatrix,
        & redMatrix);

score scoreMatrix(A0,A8,A9);

void bootSplash();

Game game(6,7,4,5, & greenMatrix, & redMatrix );


#endif	/* PROJECT_H */

