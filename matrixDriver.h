/*
 * File:   matrixDriver.h
 * Author: on8tom
 *
 * Created on February 15, 2013, 11:50 PM
 *
 *
 */

#ifndef MATRIXDRIVER_H
#define	MATRIXDRIVER_H

#include <Arduino.h>


class matrix{
private:
  uint8_t m[8];

public:
  matrix(void);

  void writeRow(uint8_t index, uint8_t data);
  void writeColumn(uint8_t index, uint8_t data);
  void writeXY(uint8_t x, uint8_t y, uint8_t data);
  void wipe(void);

  uint8_t readRow(uint8_t index);
  uint8_t readColumn(uint8_t index);
  uint8_t readXY(uint8_t x, uint8_t y);

  friend class matrixDriver;
  friend class doubleFrameBuffer;
};

class doubleFrameBuffer{
public:
  matrix* playerMatrix() const {return mplayerMatrix;};
  matrix* displayMatrix() const {return mdisplayMatrix;};
  void swap();
  void draw();
  doubleFrameBuffer();
private:
  matrix* mplayerMatrix;
  matrix* mdisplayMatrix;
  matrix m0, m1;
};

class matrixDriver {
public:
  enum direction_t{
    left,
    right,
  };
  matrixDriver(
          uint8_t commonPin,
          uint8_t greenPin,
          uint8_t redPin,
          doubleFrameBuffer* greenMatrix,
          doubleFrameBuffer* redMatrix);

  void drive(void);
  //void notify(void);
private:
  volatile uint8_t* PORTgreen;
  volatile uint8_t* PORTred;
  volatile uint8_t* PORTcommon;

  doubleFrameBuffer* _greenMatrix;
  doubleFrameBuffer* _redMatrix;

  enum state_t{
    nothing,
    rotateFalling,
  };

  state_t _state;

};
/*
 * 00 01 ... 06 07
 * 10 11 ... 16 17
 * ...............
 * 60 61 ... 66 67
 * 70 71 ... 76 77
 *
 * row : 0 top 7 bottom
 * column : 0 right 7 left
 */


#endif	/* MATRIXDRIVER_H */

