/*
 * File:   score.cpp
 * Author: on8tom
 *
 * Created on February 16, 2013, 7:16 PM
 */

#include "score.h"

score::score(uint8_t graphPin, uint8_t greenPin, uint8_t redpin) {
  greenScore = 0;
  redScore = 0;

  _greenPin = graphPin;
  _redPin = redpin;
  _greenPin = greenPin;

  pinMode(_greenPin,OUTPUT);
  pinMode(_redPin,OUTPUT);

  uint8_t arduinoPort = digitalPinToPort(graphPin);
  _barGraphPort = portOutputRegister(arduinoPort);

  //make all output (ddr high)
  * portModeRegister(arduinoPort) = 0xff;
}

void score::drive(){
  if(_side){
    digitalWrite(_greenPin,0);
    *_barGraphPort = (0x01 << redScore +1) -2;
    digitalWrite(_redPin,1);
  }
  else{
    digitalWrite(_redPin,0);
    *_barGraphPort = (0x01 << greenScore +1) -2;
    digitalWrite(_greenPin,1);
  }
  _side ++;
}